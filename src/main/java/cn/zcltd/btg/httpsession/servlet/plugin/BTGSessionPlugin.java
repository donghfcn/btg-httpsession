package cn.zcltd.btg.httpsession.servlet.plugin;

import cn.zcltd.btg.httpsession.BTGSessionDao;
import cn.zcltd.btg.httpsession.BTGSessionIdGenerator;
import cn.zcltd.btg.httpsession.kit.SessionKit;
import com.jfinal.plugin.IPlugin;

/**
 * BTG httpsession jfinal插件
 */
public class BTGSessionPlugin implements IPlugin {

    public BTGSessionPlugin() {

    }

    public BTGSessionPlugin(BTGSessionDao sessionDao) {
        SessionKit.setSessionDao(sessionDao);
    }

    @Override
    public boolean start() {
        return true;
    }

    @Override
    public boolean stop() {
        return true;
    }

    public void setSessionDao(BTGSessionDao sessionDao) {
        SessionKit.setSessionDao(sessionDao);
    }

    public void setUseCache(boolean isUseCache) {
        SessionKit.getSessionContext().setUseCache(isUseCache);
    }

    public void setSessionIdKey(String sessionIdKey) {
        SessionKit.setSessionIdKey(sessionIdKey);
    }

    public void setSessionIdGenerator(BTGSessionIdGenerator sessionIdGenerator) {
        SessionKit.setSessionIdGenerator(sessionIdGenerator);
    }

    public void setSessionTimeoutSeconds(int sessionTimeoutSeconds) {
        SessionKit.setSessionTimeoutSeconds(sessionTimeoutSeconds);
    }

    public void setMaxClearTimeoutSeconds(int maxClearTimeoutSeconds) {
        SessionKit.setMaxClearTimeoutSeconds(maxClearTimeoutSeconds);
    }

    public void setDisableSimpleSSO(boolean isDisableSimpleSSO) {
        SessionKit.setDisableSimpleSSO(isDisableSimpleSSO);
    }
}