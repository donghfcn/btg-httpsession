package cn.zcltd.btg.httpsession;

/**
 * session id生成器
 */
public interface BTGSessionIdGenerator {

    /**
     * 生成session id
     *
     * @return String
     */
    public String genaeratorSessionId();
}