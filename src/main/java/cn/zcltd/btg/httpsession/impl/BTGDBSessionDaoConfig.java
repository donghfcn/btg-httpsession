package cn.zcltd.btg.httpsession.impl;

import cn.zcltd.btg.core.exception.BtgRuntimeException;
import cn.zcltd.btg.sutil.EmptyUtil;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.DbPro;

/**
 * db session dao 所需环境配置
 */
public class BTGDBSessionDaoConfig {

    private String dbConfigName; //jfinal配置数据源名称
    private String tableName = "_btgsession"; // 存放session的表名
    private String sessionIdColumnName = "si"; //存放sessionId的列名
    private String sessionObjColumnName = "so"; //存放sessionObj对象的列名
    private String sessionLastActiveTimeColumnName = "lat"; //存放最后使用时间列名
    private String sessionMaxInactiveIntervalColumnName = "mi"; //最大空闲时间，即session的失效时间

    public BTGDBSessionDaoConfig() {

    }

    public BTGDBSessionDaoConfig(String dbConfigName) {
        this.dbConfigName = dbConfigName;
    }

    /**
     * 获取jfinalDb操作类
     *
     * @return DbPro
     */
    public DbPro getDbPro() {
        DbPro dbPro;
        if (EmptyUtil.isEmpty(this.dbConfigName)) {
            dbPro = Db.use();
            if (EmptyUtil.isEmpty(dbPro)) {
                throw new BtgRuntimeException("ActiveRecordPlugin default is not exists");
            }
        } else {
            dbPro = Db.use(this.dbConfigName);
            if (EmptyUtil.isEmpty(dbPro)) {
                throw new BtgRuntimeException("ActiveRecordPlugin name " + this.getDbConfigName() + " is not exists");
            }
        }
        return dbPro;
    }

    public String getDbConfigName() {
        return dbConfigName;
    }

    public void setDbConfigName(String dbConfigName) {
        if (EmptyUtil.isNotEmpty(dbConfigName)) {
            this.dbConfigName = dbConfigName;
        }
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        if (EmptyUtil.isNotEmpty(tableName)) {
            this.tableName = tableName;
        }
    }

    public String getSessionIdColumnName() {
        return sessionIdColumnName;
    }

    public void setSessionIdColumnName(String sessionIdColumnName) {
        if (EmptyUtil.isNotEmpty(sessionIdColumnName)) {
            this.sessionIdColumnName = sessionIdColumnName;
        }
    }

    public String getSessionObjColumnName() {
        return sessionObjColumnName;
    }

    public void setSessionObjColumnName(String sessionObjColumnName) {
        if (EmptyUtil.isNotEmpty(sessionObjColumnName)) {
            this.sessionObjColumnName = sessionObjColumnName;
        }
    }

    public String getSessionLastActiveTimeColumnName() {
        return sessionLastActiveTimeColumnName;
    }

    public void setSessionLastActiveTimeColumnName(String sessionLastActiveTimeColumnName) {
        this.sessionLastActiveTimeColumnName = sessionLastActiveTimeColumnName;
    }

    public String getSessionMaxInactiveIntervalColumnName() {
        return sessionMaxInactiveIntervalColumnName;
    }

    public void setSessionMaxInactiveIntervalColumnName(String sessionMaxInactiveIntervalColumnName) {
        this.sessionMaxInactiveIntervalColumnName = sessionMaxInactiveIntervalColumnName;
    }
}