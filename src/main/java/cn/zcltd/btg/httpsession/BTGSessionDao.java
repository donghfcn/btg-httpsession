package cn.zcltd.btg.httpsession;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * session存储器
 */
public interface BTGSessionDao extends Serializable {

    /**
     * 保存一个session
     *
     * @param session BTGSession
     */
    public void saveSession(BTGSession session);

    /**
     * 根据session id删除一个session
     *
     * @param sessionId String
     */
    public void deleteSession(String sessionId);

    /**
     * 根据session id获取一个session
     *
     * @param sessionId String
     * @return BTGSession
     */
    public BTGSession getSession(String sessionId);

    /**
     * 根据session id刷新一个session
     *
     * @param session BTGSession
     */
    public void refreshSession(BTGSession session);

    /**
     * 激活session
     *
     * @param session BTGSession
     */
    public void active(BTGSession session);

    /**
     * 获取所有session
     * 注意：此api效率较低，不推荐频繁使用
     *
     * @return Hashtable
     */
    public Hashtable<String, BTGSession> getSessions();

    /**
     * 清理超时的session
     */
    public void clearTimeout();

    /**
     * 清空session容器
     */
    public void clear();
}