package cn.zcltd.btg.httpsession.kit;

import cn.zcltd.btg.httpsession.BTGSession;
import cn.zcltd.btg.httpsession.BTGSessionDao;
import cn.zcltd.btg.httpsession.BTGSessionIdGenerator;
import cn.zcltd.btg.httpsession.impl.BTGStandardSessionContext;

import javax.servlet.ServletContext;
import java.util.Hashtable;

/**
 * session操作工具类，提供BTGStandardSessionContext的静态访问接口
 */
public class SessionKit {

    /**
     * 启用二级缓存
     */
    public static void enableCache() {
        BTGStandardSessionContext.getSessionContext().enableCache();
    }

    /**
     * 禁用二级缓存
     */
    public static void disableCache() {
        BTGStandardSessionContext.getSessionContext().disableCache();
    }

    /**
     * 获取SessionContext
     *
     * @return BTGStandardSessionContext
     */
    public static BTGStandardSessionContext getSessionContext() {
        return BTGStandardSessionContext.getSessionContext();
    }

    /**
     * 获取SessionContext
     *
     * @param servletContext servletContext
     * @return BTGStandardSessionContext
     */
    public static BTGStandardSessionContext getSessionContext(ServletContext servletContext) {
        return BTGStandardSessionContext.getSessionContext(servletContext);
    }

    /**
     * 获取SessionContext
     *
     * @param servletContext servletContext
     * @param sessionDao     sessionDao
     * @return BTGStandardSessionContext
     */
    public static BTGStandardSessionContext getSessionContext(ServletContext servletContext, BTGSessionDao sessionDao) {
        return BTGStandardSessionContext.getSessionContext(servletContext, sessionDao);
    }

    /**
     * 添加一个session
     *
     * @param session BTGSession
     */
    public static void addSession(BTGSession session) {
        BTGStandardSessionContext.getSessionContext().addSession(session);
    }

    /**
     * 根据session id移除一个session
     *
     * @param sessionId String
     */
    public static void removeSession(String sessionId) {
        BTGStandardSessionContext.getSessionContext().removeSession(sessionId);
    }

    /**
     * 根据session id获取一个session
     *
     * @param sessionId String
     * @return String
     */
    public static BTGSession getSession(String sessionId) {
        return BTGStandardSessionContext.getSessionContext().getSession(sessionId);
    }

    /**
     * 获取一个新的session
     *
     * @return BTGSession
     */
    public static BTGSession getNewSession() {
        return BTGStandardSessionContext.getSessionContext().getNewSession();
    }

    /**
     * 刷新一个session
     *
     * @param session BTGSession
     */
    public static void refreshSession(BTGSession session) {
        BTGStandardSessionContext.getSessionContext().refreshSession(session);
    }

    /**
     * 激活session
     *
     * @param session session
     */
    public static void active(BTGSession session) {
        BTGStandardSessionContext.getSessionContext().active(session);
    }

    /**
     * 获取所有session
     *
     * @return Hashtable
     */
    public static Hashtable<String, BTGSession> getSessions() {
        return BTGStandardSessionContext.getSessionContext().getSessions();
    }

    /**
     * 清理超时的session
     */
    public static void clearTimeout() {
        BTGStandardSessionContext.getSessionContext().clearTimeout();
    }

    /**
     * session清理算法机制：
     * 1、设置清理周期，默认为30分钟；
     * 2、非绝对清理，当触发了addSession、removeSession、refreshSession、getSession、getNewSession、active时触发清理机制；
     * 3、通过调用clearTimeout手动清理；
     */
    public static void checkSessionTimeout() {
        BTGStandardSessionContext.getSessionContext().checkSessionTimeout();
    }

    /**
     * 清空session容器
     */
    public static void clear() {
        BTGStandardSessionContext.getSessionContext().clear();
    }

    /**
     * 获取session id名称key
     *
     * @return String
     */
    public static String getSessionIdKey() {
        return BTGStandardSessionContext.getSessionContext().getSessionIdKey();
    }

    /**
     * 设置session id名称key
     *
     * @param sessionIdKey String
     */
    public static void setSessionIdKey(String sessionIdKey) {
        BTGStandardSessionContext.getSessionContext().setSessionIdKey(sessionIdKey);
    }

    /**
     * 获取session超时时间
     *
     * @return int
     */
    public static int getSessionTimeoutSeconds() {
        return BTGStandardSessionContext.getSessionContext().getSessionTimeoutSeconds();
    }

    /**
     * 设置session超时时间
     *
     * @param sessionTimeoutSeconds long
     */
    public static void setSessionTimeoutSeconds(int sessionTimeoutSeconds) {
        BTGStandardSessionContext.getSessionContext().setSessionTimeoutSeconds(sessionTimeoutSeconds);
    }

    /**
     * 获取session cookie默认path
     *
     * @return String String
     */
    public static String getSessionCookiePath() {
        return BTGStandardSessionContext.getSessionContext().getSessionCookiePath();
    }

    /**
     * 设置session cookie默认path
     *
     * @param sessionCookiePath session cookie默认path
     */
    public static void setSessionCookiePath(String sessionCookiePath) {
        BTGStandardSessionContext.getSessionContext().setSessionCookiePath(sessionCookiePath);
    }

    /**
     * 获取是否禁用简单单点登录
     *
     * @return boolean boolean
     */
    public static boolean isDisableSimpleSSO() {
        return BTGStandardSessionContext.getSessionContext().isDisableSimpleSSO();
    }

    /**
     * 设置是否禁用简单单点登录
     *
     * @param disableSimpleSSO disableSimpleSSO
     */
    public static void setDisableSimpleSSO(boolean disableSimpleSSO) {
        BTGStandardSessionContext.getSessionContext().setDisableSimpleSSO(disableSimpleSSO);
    }

    /**
     * 获取过期session清理机制触发周期(毫秒)
     *
     * @return int
     */
    public static int getMaxClearTimeoutSeconds() {
        return BTGStandardSessionContext.getSessionContext().getMaxClearTimeoutSeconds();
    }

    /**
     * 设置过期session清理机制触发周期(毫秒)
     *
     * @param maxClearTimeoutSeconds int
     */
    public static void setMaxClearTimeoutSeconds(int maxClearTimeoutSeconds) {
        BTGStandardSessionContext.getSessionContext().setMaxClearTimeoutSeconds(maxClearTimeoutSeconds);
    }

    /**
     * 获取sessionId生成器
     *
     * @return BTGSessionIdGenerator
     */
    public static BTGSessionIdGenerator getSessionIdGenerator() {
        return BTGStandardSessionContext.getSessionContext().getSessionIdGenerator();
    }

    /**
     * 设置sessionId生成器
     *
     * @param sessionIdGenerator BTGSessionIdGenerator
     */
    public static void setSessionIdGenerator(BTGSessionIdGenerator sessionIdGenerator) {
        BTGStandardSessionContext.getSessionContext().setSessionIdGenerator(sessionIdGenerator);
    }

    /**
     * 获取session存储器
     *
     * @return BTGSessionDao
     */
    public static BTGSessionDao getSessionDao() {
        return BTGStandardSessionContext.getSessionContext().getSessionDao();
    }

    /**
     * 设置session存储器
     *
     * @param sessionDao BTGSessionDao
     */
    public static void setSessionDao(BTGSessionDao sessionDao) {
        BTGStandardSessionContext.getSessionContext().setSessionDao(sessionDao);
    }

    /**
     * 获取ServletContext
     *
     * @return ServletContext
     */
    public static ServletContext getServletContext() {
        return BTGStandardSessionContext.getSessionContext().getServletContext();
    }

    /**
     * 设置ServletContext
     *
     * @param servletContext ServletContext
     */
    public static void setServletContext(ServletContext servletContext) {
        BTGStandardSessionContext.getSessionContext().setServletContext(servletContext);
    }
}