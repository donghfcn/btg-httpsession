drop table if exists _btgsession;

/*==============================================================*/
/* Table: _btgsession                                           */
/*==============================================================*/
create table _btgsession
(
   si                   varchar(32) not null comment 'session id',
   so                   blob comment 'session对象',
   lat                  bigint comment '最后访问时间',
   mi                   bigint comment '最大空闲时间，即session的失效时间',
   primary key (si)
);

alter table _btgsession comment '分布式session存储';
